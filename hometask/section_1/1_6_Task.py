import sys

"""
6. Get string as input argument and verify it contains: letter a, any digit, word 'apple'
Input: aaabbbappleaaa
Output: Input string contains letter a: True, any digit: False, word 'apple': True
Input: plebb5b
Output: Input string contains letter a: False, any digit: True, word 'apple': False
"""

variable = str(sys.argv[1])
varA = 'a' in variable
varDigit = any(char.isdigit() for char in variable)
varApple = 'apple' in variable
print('Input string contains letter a:', varA, ', any digit:', varDigit, "word 'apple':", varApple)
