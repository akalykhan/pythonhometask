import sys

"""
4. Get 2 numbers as input arguments and print result of devision with 2 fractional digits:
Input: 4 5
Output: 0.80
"""

a = float(sys.argv[1])
b = float(sys.argv[2])
print(format(a / b, '.2f'))
