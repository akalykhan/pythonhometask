import sys

"""
5. Get list arguments in sys.argv and return all of them as one sentence starting with Capital letter and ending with dot.
Input: "tom was good boy" "alice likes apples"
Output: Tom as good boy. Alice likes apples.
"""

a = ''
for arg in sys.argv[1:]:
    a += arg.capitalize() + '. '
print(a)
