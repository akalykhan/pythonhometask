from hometask.Utils import get_arg_list

"""
3. Get one input string argument and print its letters in table format: 3 letters per line delimited by 4 spaces:
Input: comprehensive
Output:
c    o    m
p    r    e
h    e    n    
s    i    v
e
"""

input_string = get_arg_list()[1]
output_string = ''
for idx, val in enumerate(input_string):
    if (idx + 1) % 3 == 0:
        output_string += val + '\n'
    else:
        output_string += val + '\t'
print(output_string)
