import sys

"""
2. Get one input string argument and cut one word from the beginning and from the end:
Input: apple orange tomato potato
Output: orange tomato - done
"""

inp = sys.argv[1]
li = inp.split(maxsplit=1)[1]
print(li.rsplit(maxsplit=1)[0])
