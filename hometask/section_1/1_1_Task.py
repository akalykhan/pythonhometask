from hometask.Utils import get_arg_list_int

"""
1. Get input arguments (sys.argv) and print them and their sum:
Input: 1 2 5 6
Output: Got 1, 2, 5, 6. Sum is 14.
"""

list_args = get_arg_list_int()
sum_args = 0
string_value = ''
for value in list_args:
    sum_args += value
    string_value += str(value) + ', '

print('Got: ' + string_value[:-2] + '. Sum is {}'.format(sum_args))
