from hometask.Utils import print_list

'''
Найти какое-нибудь слово, начинающееся с буквы а.
Input: this is   my world around
Output: around
'''

st = 'thit is   mym world around'

li = st.split(' ')
li_a = []

for x in li:
    if len(x) > 0 and x[0] == 'a':
        li_a.append(x)
print_list(li_a)
