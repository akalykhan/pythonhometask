'''
2. Подсчитать количество букв а в последнем слове данной последовательности
Input: this is   my world around
Output: 1
'''

st = 'this is   my world around'

li = st.split(' ')
length = len(li)
count = 0
# last_word=
for x in li[len(li) - 1]:
    if x == 'a':
        count += 1
print(count)
