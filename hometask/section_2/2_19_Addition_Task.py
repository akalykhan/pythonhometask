from hometask.Utils import get_arg_list

'''
b) Даны натуральное число n, символы s1 , ..., sn. Группы символов, разделенные
пробелами (одним или несколькими) и не содержащие пробелы внутри себя, будем
называть словами.
1. Подсчитать количество слов в данной последовательности
Input: this is   my word
Output: 4
'''

st = get_arg_list()[0]

li = st.split(' ')
a = 0
for x in li:
    if x != '':
        a += 1
print(a)
