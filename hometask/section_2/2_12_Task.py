from hometask.Utils import get_arg_list_int

'''
12. Get 3 arguments: sum, interest and years. Return resulting sum if it was bank account (each year sum increases by interest, result is added to sum)
Input: 100 10% 3
Output: 133.1
'''

sum = 0
perc, year = get_arg_list_int()[1], get_arg_list_int()[2]

for i in range(0, year):
    sum += sum * perc / 100

print(sum)
