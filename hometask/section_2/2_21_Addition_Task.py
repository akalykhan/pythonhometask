'''
3. Найти количество слов, у которых первый и последний символы совпадают
между собой.

Input: thit is   mym world around
Output: 2
'''

st = 'thit is   mym world around'

li = st.split(' ')
count = 0
for x in li:
    if x:
        first_ch = x[0]
        if x[0] == x[-1:]:
            count += 1
print(count)
