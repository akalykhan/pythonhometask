from hometask.Utils import get_arg_list_int

"""
1. For each input argument print its square
Input: 1 5 6
Output: 1 25 64
"""

arglist = []

print([x ** 2 for x in get_arg_list_int()])
