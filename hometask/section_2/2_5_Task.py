from hometask.Utils import get_arg_list,print_ditionary

"""
5. Calculate number of different letters in input string (don't distinguish capital and lowercase letters)
Input: Awesome
Output: a:1 w:1 e:2 s:1 o:1 m:1
"""

st = str(get_arg_list()[0]).lower()
slist = set(list(st))
di = {}

for x in slist:
    di[x] = st.count(x)

print(di)
print_ditionary(di)
