from hometask.Utils import get_arg_list

"""
4. Verify input arguments are strings and print length of every string
Input:one ring to rule them all
Output:3 4 2 4 4 3
"""
li = get_arg_list()
state = True
##nli=[]

for var in li:
    bo = str(var).isalpha()
    if bo == False:
        print('Error: {} is not string'.format(var))
        state = bo
        break

if state == True:
    [print(str(x).__len__()) for x in li]
