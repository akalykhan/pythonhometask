from hometask.Utils import get_arg_list

"""
3. Verify input arguments are integers and print them in reverse order
Input: 4 5
Output: 5 4
Input: aa 55
Output: Error: aa is not integer
"""

li = get_arg_list()
state = True

for var in li:
    bo = str(var).isdigit()
    if bo == False:
        print('Error: {} is not integer'.format(var))
        state = bo
        break

if state == True:
    [print(str(x)) for x in li]
