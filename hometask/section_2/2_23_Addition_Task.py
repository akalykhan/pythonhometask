'''
5. Преобразовать данную последовательность, заменяя всякое вхождение слова
this на слово that.
Input: this is   my world around
Output: that is   my world around
'''

st = 'this is   my world around'

print(st.replace('this', 'that'))
