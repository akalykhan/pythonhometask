from hometask.Utils import get_arg_list_int

'''
13. Get number and print if it is simple
Input: 13
Output: yes
'''

val = get_arg_list_int()[0]
bol = True

if str(val).isdigit():
    if val > 2:
        for i in range(2, val - 1):
            if val % i == 0:
                print('{} is not a simple number'.format(val))
                bol = False
                break
    if bol == True:
        print('yes')
else:
    print('{} is not a simple number'.format(val))
