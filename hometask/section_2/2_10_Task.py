from hometask.Utils import get_arg_list_int

"""
10. Get numbers and print maximum difference between 2 numbers
Input: 1 4 5 33 2 9 33
Output: 31
"""

li = get_arg_list_int()
# li=[1,4,5,33,2,100,33]
a = 0

for i in range(0, len(li) - 1):
    b = li[i] - li[i + 1]
    if a < b:
        a = b

print(a)
