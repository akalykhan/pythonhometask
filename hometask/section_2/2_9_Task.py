from hometask.Utils import get_arg_list

"""
9. Get 2 strings as input. For each letter of first string, print letter of second. Extend string if needed with last letter.
Input: abcd efg
Output: ae bf cg dg
"""

a = str(get_arg_list()[0])
b = str(get_arg_list()[1])
li = []

la = len(a)
lb = len(b)

print(b)
if la > lb:
    for i in range(0, la - lb):
        b = b + b[-1:]

    for i in range(0, la):
        li.append(a[i] + b[i])

print_list(li)
