from hometask.Utils import get_arg_list_int

"""
6. Get decimal number as argument, convert is into binary format
Input: 14
Output: 1100
"""

print(bin(get_arg_list_int()[0])[2:])
