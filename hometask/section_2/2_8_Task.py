from hometask.Utils import get_arg_list

"""
8. Get 2 strings as input arguments. Print letters that exists in both strings (solve in 2 different ways)
Input: aaccbbb rrrcbaa
Output: aacb
"""
a =str(get_arg_list()[0])
b = str(get_arg_list()[1])
st = ''
for x in a:
    if x in b:
        b = b.replace(x, '',1)
        st +=x
print(st)
