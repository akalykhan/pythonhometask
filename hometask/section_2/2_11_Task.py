from hometask.Utils import get_arg_list_int

'''
11. Get year and print if it is leap year:
Input: 2000
Output: Yes, 200 is leap year
'''

year = get_arg_list_int()[0]

if year % 4 == 0:
    print('Yes, {} is leap year'.format(year))
else:
    print('No, {} is not leap year'.format(year))
