from hometask.Utils import get_arg_list

'''
a) Даны натуральное число n, символы s1 , ..., sn . Известно, что среди s1 , ..., sn есть по
крайней мере одна запятая. Найти такое натуральное i, что
1. si — первая по порядку запятая;
2. si — последняя по порядку запятая.

1: Input: sdf,g,fg
    Output: 4
2: Input: sdf,g,fg
    Output: 6
'''

st = str(get_arg_list()[0])

first_position = st.find(',') + 1

print('Decision for first comma position')
if first_position == 0:
    print('There is no comma in String')
else:
    print('First comma order is: {}'.format(first_position))

print('Decision for last comma position')
a = 0
for i in range(0, len(st)):
    if st[i] == ',':
        a = i
if a == 0:
    print('There is no comma in String')
else:
    print('Last comma order is: {}'.format(a + 1))
