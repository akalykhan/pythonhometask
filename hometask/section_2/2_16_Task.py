from hometask.Utils import get_arg_list

'''
16. Get number as input, print squared every 3rd number from 1/3 of number till 2/3 of number
Input: 16
Output: 36 81 100
'''

arg_list = get_arg_list()
