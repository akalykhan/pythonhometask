from hometask.Utils import get_arg_list

'''
15. Get strings, print strings with len=2
Input: df rtyuu ew feefe
Output: df ew
'''

arg_list = get_arg_list()

for ar in arg_list:
    if len(ar) == 2:
        print(ar)
