from hometask.Utils import print_list, get_arg_list_int

'''
17. Get number as arguments. Print in reserved order.
'''

li1 = get_arg_list_int()
li2 = get_arg_list_int()
li3 = get_arg_list_int()

print('First reserved method')
li1.reverse()
for x in li1:
    print(x)
print()

print('Second reserved method')
new_list = li2[::-1]
print_list(new_list)
print()

x = True
ch_number = len(li3) - 1
print('Third reserved method')
while x == True:
    print(li3[ch_number])
    ch_number = ch_number - 1
    if ch_number == -1:
        x = False
