import sys


def get_arg_list_int():
    argList = []
    for arg in sys.argv[1:]:
        argList.append(int(arg))
    return argList


def get_arg_list():
    argList = []
    for arg in sys.argv[1:]:
        argList.append(arg)
    return argList


def print_ditionary(self, **data):
    for kw in data:
        print(kw)


def print_list(data):
    for x in data:
        print(x)
